from PyQt5.QtCore import QObject
from PyQt5.QtCore import pyqtSignal, pyqtProperty, pyqtSlot

from Xlib import display

class WMHelper(QObject):

    hasCompositeChanged = pyqtSignal(bool)
    
    def __init__(self, parent=None):
        QObject.__init__(self, parent)

        self._has_composited = self.getWmHasComposite()

    @pyqtProperty(bool, notify=hasCompositeChanged)
    def hasComposite(self):
        return self._has_composited

    @pyqtSlot()
    def updateHasComposited(self):
        has_composited = self.getWmHasComposite()

        if has_composited != self._has_composited:
            self._has_composited = has_composited
            print("has composite changed: ", self._has_composited)
            self.hasCompositeChanged.emit(self._has_composited)

    def getWmHasComposite(self):
        dis = display.Display()
        at = dis.get_atom("_NET_WM_CM_S0")
        return dis.get_selection_owner(at) != 0

wmHelper = WMHelper()